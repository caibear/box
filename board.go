package main

import "sort"

type Board struct {
	boxes       []Box
	playerIndex int
}

func (b *Board) TestCollisions(timeDelta float32) {
	sort.Sort(b)

	for i := range b.boxes {
		for j := i + 1; j < len(b.boxes); j++ {
			if b.testCollision(i, j, timeDelta) {
				break
			}
		}

		for j := i - 1; j > 0; j-- {
			if b.testCollision(i, j, timeDelta) {
				break
			}
		}
	}
}

func (b *Board) testCollision(i, k int, timeDelta float32) bool {
	if b.boxes[i].scale < b.boxes[k].scale || (b.boxes[i].scale == b.boxes[k].scale && i < k) {
		return true
	}

	b.boxes[k].Collide(&b.boxes[i], timeDelta)
	b.boxes[i].Collide(&b.boxes[k], timeDelta)

	if Abs32(b.boxes[i].position[0]-b.boxes[k].position[0]) > b.boxes[i].scale*2 {
		return true
	}
	return false
}

func (b *Board) Less(i, j int) bool {
	return b.boxes[i].position[0] < b.boxes[j].position[0]
}

func (b *Board) Len() int {
	return len(b.boxes)
}

func (b *Board) Swap(i, j int) {
	b.boxes[i], b.boxes[j] = b.boxes[j], b.boxes[i]
	if i == b.playerIndex {
		b.playerIndex = j
	} else if j == b.playerIndex {
		b.playerIndex = i
	}
}

func (b *Board) Add(box Box) {
	b.boxes = append(b.boxes, box)
}

func (b *Board) AddPlayer(box Box) {
	b.playerIndex = len(b.boxes)
	b.Add(box)
}

func (b *Board) Player() *Box {
	return &b.boxes[b.playerIndex]
}
