package main

import (
	"fmt"
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"log"
	"runtime"
	"time"
)

var window Window

type Window struct {
	win *glfw.Window
}

func (window *Window) Create(name string, fullscreen bool) {
	runtime.LockOSThread()

	var err error
	window.win, err = createGLFWWindow(name, fullscreen)
	if err != nil {
		log.Fatal(err)
	}
	initOpenGL()
}

func (window *Window) Start(frame func(timeDelta float64, t float64)) {
	// time
	lastFrame := time.Now()
	var t float64

	for !window.win.ShouldClose() {
		// time step
		currentFrame := time.Now()
		duration := currentFrame.Sub(lastFrame)
		lastFrame = currentFrame
		timeDelta := duration.Seconds()
		t += timeDelta

		frame(timeDelta, t)

		// draw frame
		window.win.SwapBuffers()

		// get close window and keys
		glfw.PollEvents()
	}

	window.end()
}

func (window *Window) end() {
	glfw.Terminate()
}

func createGLFWWindow(name string, fullscreen bool) (*glfw.Window, error) {
	if err := glfw.Init(); err != nil {
		panic(err)
	}

	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

	monitor := glfw.GetPrimaryMonitor()
	videoMode := monitor.GetVideoMode()

	if !fullscreen {
		monitor = nil
	}

	win, err := glfw.CreateWindow(videoMode.Width, videoMode.Height, name, monitor, nil)
	if err != nil {
		return nil, err
	}

	win.MakeContextCurrent()
	return win, nil
}

func initOpenGL() {
	if err := gl.Init(); err != nil {
		panic(err)
	}
	version := gl.GoStr(gl.GetString(gl.VERSION))
	log.Println("OpenGL version", version)
}

func CreateProgram(vertexSource, fragmentSource string) (uint32, error) {
	vertexShader, err := compileShader(vertexSource, gl.VERTEX_SHADER)
	if err != nil {
		return 0, err
	}

	fragmentShader, err := compileShader(fragmentSource, gl.FRAGMENT_SHADER)
	if err != nil {
		return 0, err
	}

	program := gl.CreateProgram()
	gl.AttachShader(program, vertexShader)
	gl.AttachShader(program, fragmentShader)
	gl.LinkProgram(program)

	var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		infoLog := string(make([]byte, logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(infoLog))

		return 0, fmt.Errorf("failed to compile %v", infoLog)
	}
	return program, nil
}

func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	cString, free := getStringPtr(source)
	gl.ShaderSource(shader, 1, cString, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		infoLog := string(make([]byte, logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(infoLog))

		return 0, fmt.Errorf("failed to compile %v", infoLog)
	}

	return shader, nil
}
