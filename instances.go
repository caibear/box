package main

import (
	"github.com/go-gl/gl/v4.1-core/gl"
)

type Instances struct {
	Instances     []float32
	InstanceCount int32
	geometry      *Geometry
	vao           uint32
	vbo           uint32
	initialized   bool
}

func (instances *Instances) init() {
	gl.GenBuffers(1, &instances.vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, instances.vbo)

	gl.GenVertexArrays(1, &instances.vao)
	gl.BindVertexArray(instances.vao)

	var attrib uint32
	var offset int

	gl.BindBuffer(gl.ARRAY_BUFFER, instances.geometry.vbo)
	gl.EnableVertexAttribArray(attrib)
	gl.VertexAttribPointer(attrib, 3, gl.FLOAT, false, 3*4, gl.PtrOffset(offset))
	attrib++
	offset += 3 * 4

	offset = 0
	gl.BindBuffer(gl.ARRAY_BUFFER, instances.vbo)
	for i := 0; i < 4; i++ {
		gl.EnableVertexAttribArray(attrib)
		gl.VertexAttribPointer(attrib, 4, gl.FLOAT, false, 19*4, gl.PtrOffset(offset))
		gl.VertexAttribDivisor(attrib, 1)
		attrib++
		offset += 4 * 4
	}

	gl.EnableVertexAttribArray(attrib)
	gl.VertexAttribPointer(attrib, 3, gl.FLOAT, false, 19*4, gl.PtrOffset(offset))
	gl.VertexAttribDivisor(attrib, 1)
	attrib++
	offset += 3 * 4

	instances.initialized = true
}

func (instances *Instances) BufferData() {
	if !instances.initialized {
		instances.init()
	}

	instances.InstanceCount = int32(len(instances.Instances))

	gl.BindBuffer(gl.ARRAY_BUFFER, instances.vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(instances.Instances), gl.Ptr(instances.Instances), gl.DYNAMIC_DRAW)
}

func (instances *Instances) Bind() {
	if instances.initialized {
		gl.BindVertexArray(instances.vao)
	}
}

func (instances *Instances) Draw() {
	if instances.initialized {
		gl.DrawArraysInstanced(gl.TRIANGLES, 0, instances.geometry.vertexCount, instances.InstanceCount)
	}
}

func (instances *Instances) BindAndDraw() {
	instances.Bind()
	instances.Draw()
}
