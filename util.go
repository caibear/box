package main

import (
	"github.com/go-gl/gl/v4.1-core/gl"
	"math"
)

func getStringPtr(s string) (**uint8, func()) {
	return gl.Strs(s + "\x00")
}

func getString(s string) (*uint8, func()) {
	c, free := getStringPtr(s)
	return *c, free
}

func Min32(a, b float32) float32 {
	if a < b {
		return a
	}
	return b
}

func Max32(a, b float32) float32 {
	if a > b {
		return a
	}
	return b
}

func Abs32(x float32) float32 {
	return math.Float32frombits(math.Float32bits(x) &^ (1 << 31))
}

func Sign32(x float32) float32 {
	return math.Float32frombits(math.Float32bits(1.0) | math.Float32bits(x)&(1 << 31))
}