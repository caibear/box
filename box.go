package main

import (
	"github.com/go-gl/mathgl/mgl32"
	"math"
	"math/rand"
)

type Box struct {
	position     mgl32.Vec2
	velocity     mgl32.Vec2
	acceleration mgl32.Vec2
	scale        float32
	color        mgl32.Vec3
}

func (box *Box) Control([4]bool) {
	const controlAcceleration = playerSpeed * scaleMultiplier

	var acc mgl32.Vec2
	if controls[0] && !controls[1] {
		acc[1] = controlAcceleration
	} else if controls[1] {
		acc[1] = -controlAcceleration
	}

	if controls[2] && !controls[3] {
		acc[0] = -controlAcceleration
	} else if controls[3] {
		acc[0] = controlAcceleration
	}

	box.acceleration = acc
}

func (box *Box) Update(timeDelta float32) {
	acceleration := box.acceleration.Add(box.velocity.Mul(-friction))
	box.velocity = box.velocity.Add(acceleration.Mul(timeDelta))
	box.position = box.position.Add(box.velocity.Mul(timeDelta))

	scale := box.Scale()

	box.position[0] = mgl32.Clamp(box.position[0], -boardWidth/2+scale[0]/2, boardWidth/2-scale[0]/2)
	box.position[1] = mgl32.Clamp(box.position[1], -boardHeight/2+scale[1]/2, boardHeight/2-scale[1]/2)
}

func (box *Box) Collide(other *Box, timeDelta float32) {
	distance := box.position.Sub(other.position)

	signs := mgl32.Vec2{Sign32(distance[0]), Sign32(distance[1])}
	distance = mgl32.Vec2{Abs32(distance[0]), Abs32(distance[1])}

	collision := distance.Sub(box.Scale().Add(other.Scale()).Mul(0.5))
	if collision[0] > 0 || collision[1] > 0 {
		return
	}
	collision[0], collision[1] = collision[1]*signs[0], collision[0]*signs[1]

	massDiff := (other.scale) / (box.scale)
	box.velocity = box.velocity.Add(collision.Mul(timeDelta * massDiff * -5.0))
}

func (box *Box) RandomColor() {
	t := rand.Float64()
	box.color = mgl32.Vec3{
		float32(math.Mod(t, 1.0)),
		float32(math.Mod(t+1.0/3.0, 1.0)),
		float32(math.Mod(t+2.0/3.0, 1.0)),
	}
}

func (box *Box) RandomScale() {
	box.scale = rand.Float32()*boxSize + boxSize
	box.scale *= scaleMultiplier
}

func (box *Box) RandomPosition() {
	box.position = mgl32.Vec2{rand.Float32()*boardWidth - boardWidth/2, rand.Float32()*boardHeight - boardHeight/2}
}

func (box *Box) Scale() mgl32.Vec2 {
	x, y := Abs32(box.velocity[0]), Abs32(box.velocity[1])
	return mgl32.Vec2{
		box.scale / Max32(1.0, y*0.15/scaleMultiplier-x*0.04/scaleMultiplier),
		box.scale / Max32(1.0, x*0.15/scaleMultiplier-y*0.04/scaleMultiplier),
	}
}

func (box *Box) Transform() mgl32.Mat4 {
	scale := box.Scale()
	matrix := mgl32.Translate3D(box.position[0], box.position[1], box.scale)
	matrix = matrix.Mul4(mgl32.Scale3D(scale[0], scale[1], 1.0))
	return matrix
}
