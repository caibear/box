package main

import (
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/go-gl/mathgl/mgl32"
	"log"
)

const (
	boxVertexShaderSource = `
#version 300 es

layout (location = 0) in vec3 position;
layout (location = 1) in mat4 instanceMatrix;
layout (location = 5) in vec3 instanceColor;

out vec3 vColor;

uniform mat4 projectionViewMatrix;
void main() {
    gl_Position = projectionViewMatrix * instanceMatrix * vec4(position, 1.0);
    vColor = instanceColor;
}
`

	boxFragmentShaderSource = `
#version 300 es
precision mediump float;

in vec3 vColor;

out vec4 frag_colour;

void main() {
    frag_colour = vec4(vColor, 1.0);
}
`
)

const (
	scaleMultiplier = 0.025
	boxCount        = 10000
	friction        = 5.0
	boardWidth      = 2.0 * (16.0 / 9.0)
	boardHeight     = 2.0
	playerSize      = 5.0
	playerSpeed     = 75.0
	boxSize         = 0.2
)

var square = []float32{
	-0.5, 0.5, 0,
	-0.5, -0.5, 0,
	0.5, -0.5, 0,

	-0.5, 0.5, 0,
	0.5, 0.5, 0,
	0.5, -0.5, 0,
}

var (
	boxProgram                  uint32
	projectionViewMatrixUniform int32
)

var (
	squareGeometry  = Geometry{Vertices: square}
	squareInstances = Instances{geometry: &squareGeometry}
)

var controls [4]bool
var board Board

func main2() {
	window.Create("Game", true)

	// gl data
	squareGeometry.BufferData()

	var err error
	boxProgram, err = CreateProgram(boxVertexShaderSource, boxFragmentShaderSource)
	if err != nil {
		log.Fatal(err)
	}

	uniformName, free := getString("projectionViewMatrix")
	projectionViewMatrixUniform = gl.GetUniformLocation(boxProgram, uniformName)
	free()

	gl.Enable(gl.DEPTH_TEST)
	gl.DepthRangef(0.0, 1.0)
	gl.ClearColor(0, 0, 0, 1)

	// board
	board.AddPlayer(Box{
		scale: playerSize * scaleMultiplier,
		color: mgl32.Vec3{1.0, 1.0, 1.0},
	})

	for i := 1; i < boxCount; i++ {
		var box Box
		box.RandomColor()
		box.RandomPosition()
		box.RandomScale()
		board.Add(box)
	}

	// window
	window.win.SetKeyCallback(keyCallback)
	window.Start(frame)
}

func frame(timeDelta, t float64) {
	board.Player().Control(controls)

	// updates
	squareInstances.Instances = squareInstances.Instances[:0]

	for i := range board.boxes {
		board.boxes[i].Update(float32(timeDelta))
		matrix := board.boxes[i].Transform()
		squareInstances.Instances = append(squareInstances.Instances, matrix[:]...)
		squareInstances.Instances = append(squareInstances.Instances,
			board.boxes[i].color[0],
			board.boxes[i].color[1],
			board.boxes[i].color[2],
		)
	}

	squareInstances.BufferData()

	board.TestCollisions(float32(timeDelta))

	// clear old frame
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	// draw player
	projectionViewMatrix := mgl32.Scale3D(9.0/16.0, 1.0, 1.0)

	gl.UseProgram(boxProgram)
	gl.UniformMatrix4fv(projectionViewMatrixUniform, 1, true, &projectionViewMatrix[0])

	squareInstances.BindAndDraw()
}

func keyCallback(_ *glfw.Window, key glfw.Key, _ int, action glfw.Action, _ glfw.ModifierKey) {
	switch key {
	case glfw.KeyW, glfw.KeyUp:
		controls[0] = action != glfw.Release
	case glfw.KeyA, glfw.KeyLeft:
		controls[2] = action != glfw.Release
	case glfw.KeyS, glfw.KeyDown:
		controls[1] = action != glfw.Release
	case glfw.KeyD, glfw.KeyRight:
		controls[3] = action != glfw.Release
	}
}
