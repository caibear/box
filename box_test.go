package main

import (
	"sort"
	"testing"
)

func (b *Board) RandomBoard(size int) {
	b.boxes = make([]Box, size)
	b.playerIndex = -1

	for i := range b.boxes {
		b.boxes[i].RandomPosition()
		b.boxes[i].RandomScale()
	}

	sort.Sort(b)
}

func BenchmarkTestCollisions(b *testing.B) {
	const timeDelta = 0.1

	b.StopTimer()
	var board Board
	board.RandomBoard(1000)
	b.StartTimer()

	for c := 0; c < b.N; c++ {
		i := c % len(board.boxes)
		for j := i + 1; j < len(board.boxes); j++ {
			if board.testCollision(i, j, timeDelta) {
				break
			}
		}

		for j := i - 1; j > 0; j-- {
			if board.testCollision(i, j, timeDelta) {
				break
			}
		}
	}
}
