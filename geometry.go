package main

import (
	"github.com/go-gl/gl/v4.1-core/gl"
)

type Geometry struct {
	Vertices    []float32
	vertexCount int32
	vao         uint32
	vbo         uint32
	initialized bool
}

func (geometry *Geometry) init() {
	gl.GenBuffers(1, &geometry.vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, geometry.vbo)

	gl.GenVertexArrays(1, &geometry.vao)
	gl.BindVertexArray(geometry.vao)

	gl.BindBuffer(gl.ARRAY_BUFFER, geometry.vbo)
	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 3*4, nil)

	geometry.initialized = true
}

func (geometry *Geometry) BufferData() {
	if !geometry.initialized {
		geometry.init()
	}

	geometry.vertexCount = int32(len(geometry.Vertices))

	gl.BindBuffer(gl.ARRAY_BUFFER, geometry.vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(geometry.Vertices), gl.Ptr(geometry.Vertices), gl.STATIC_DRAW)
}

func (geometry *Geometry) Bind() {
	if geometry.initialized {
		gl.BindVertexArray(geometry.vao)
	}
}

func (geometry *Geometry) Draw() {
	if geometry.initialized {
		gl.DrawArrays(gl.TRIANGLES, 0, geometry.vertexCount)
	}
}

func (geometry *Geometry) BindAndDraw() {
	geometry.Bind()
	geometry.Draw()
}
